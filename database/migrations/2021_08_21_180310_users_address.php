<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UsersAddress', function (Blueprint $table)
        {
            $table->increments('addressId');
            $table->string('address',30)->nullable();
            $table->string('number', 5)->nullable();
            $table->string('neighborhood', 20)->nullable();
            $table->string('city',20)->nullable();
            $table->string('state',20)->nullable();
            $table->string('postalCode',8)->nullable();
            $table->string('country',20)->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('UsersAddress');
    }
}
