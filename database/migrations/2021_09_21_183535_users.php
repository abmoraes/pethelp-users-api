<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Users extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Users', function (Blueprint $table)
        {
            $table->increments('userId');
            $table->string('document', 14)->nullable();
            $table->date('dateBirth')->nullable()->nullable();
            $table->string('gender',1)->nullable();
            $table->string('firstName',15)->nullable();
            $table->string('lastName',50)->nullable();
            $table->string('telefone',11)->nullable();
            $table->boolean('ong')->nullable();
            $table->integer('addressId')->unsigned();
            $table->foreign('addressId')->references('addressId')->on('UsersAddress');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('Users');
    }
}
