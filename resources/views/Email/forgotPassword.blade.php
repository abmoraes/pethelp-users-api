@component('mail::message')

    # Olá, {!! $name !!}!


    Está sem acesso ao nosso sistema? Vamos mudar logo essa situação porque nossos bichinhos não podem esperar.


    Clique no botão abaixo e crie uma nova senha:

@component('mail::button', ['url' => 'https://projetopet.herokuapp.com/novasenha?token='.$token.'&email='.$email])
        Redefinir senha
@endcomponent

@component('mail::footer')
    Obrigada,<br/>{{ config('app.name') }}
@endcomponent

@endcomponent
