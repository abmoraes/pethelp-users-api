<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


use App\Services\ImageService;

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/key', function() {
    return \Illuminate\Support\Str::random(32);
});
$router->group(['prefix' => 'api'], function () use ($router) {
    // Matches "/api/register
   $router->post('register', 'AuthController@register');
     // Matches "/api/login
    $router->post('login', 'AuthController@login');

    // Matches "/api/profile
    $router->get('profile', 'UsersAuthenticationController@profile');

    // Matches "/api/singleuser
    $router->get('user/{id}', 'UsersAuthenticationController@singleUser');

    // Matches "/api/users
    $router->get('users', 'UsersAuthenticationController@allUsers');

    $router->post('logout', 'AuthController@logout');

    $router->get('image/{page}/', function ($page) {

        $x  = ImageService::show("$page","./");
        return $x;
    });

    $router->post('/req-password-reset', 'ResetPwdReqController@reqForgotPassword');
    $router->post('/update-password', 'UpdatePwdController@updatePassword');

});


