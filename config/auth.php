<?php

return [
    'defaults' => [
        'guard' => 'api',
        'passwords' => 'UsersAuthentication',
    ],

    'guards' => [
        'api' => [
            'driver' => 'jwt',
            'provider' => 'UsersAuthentication',
        ],
    ],

    'providers' => [
        'UsersAuthentication' => [
            'driver' => 'eloquent',
            'model' => \App\Models\UsersAuthentication::class
        ]
        ],
        'passwords' => [
            'users' => [
                'provider' => 'UsersAuthentication',
                'table' => 'password_resets',
                'expire' => 60,
                'throttle' => 60,
            ],
          ],
        'password_timeout' => 10800,
];
