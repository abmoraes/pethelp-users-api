<?php $__env->startComponent('mail::message'); ?>
# Change Password

<?php $__env->startComponent('mail::button', ['url' => 'http://localhost:4200/update-password?token='.$token.'&email='.$x]); ?>
Reset Password
<?php echo $__env->renderComponent(); ?>

Thanks,<br>
<?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?><?php /**PATH /var/www/resources/views/Email/forgotPassword.blade.php ENDPATH**/ ?>