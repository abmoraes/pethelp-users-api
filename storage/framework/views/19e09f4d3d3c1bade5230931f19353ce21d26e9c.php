<?php $__env->startComponent('mail::message'); ?>

    # Olá, <?php echo $name; ?>!


    Está sem acesso ao nosso sistema? Vamos mudar logo essa situação porque nossos bichinhos não podem esperar.


    Clique no botão abaixo e crie uma nova senha:

<?php $__env->startComponent('mail::button', ['url' => 'https://projetopet.herokuapp.com/novasenha?'.$token.'&email='.$email]); ?>
        Redefinir senha
<?php echo $__env->renderComponent(); ?>

<?php $__env->startComponent('mail::footer'); ?>
    Obrigada,<br/><?php echo e(config('app.name')); ?>

<?php echo $__env->renderComponent(); ?>

<?php echo $__env->renderComponent(); ?>
<?php /**PATH /var/www/resources/views/Email/forgotPassword.blade.php ENDPATH**/ ?>