<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
 use Illuminate\Support\Facades\Auth;


class Controller extends BaseController
{

    /**
     * @OA\Info(
     *   title="Your Awesome Modules's API",
     *  version="1.0.0",
     *  @OA\Contact(
     *    email="developers@module.com",
     *    name="Developer Team"
     *  )
     * )
     */

    /**
     * @SWG\Property(
     *      property="id",
     *      type="integer",
     *      example="103"
     * )
     * @SWG\Property(
     *      property="status",
     *      type="string",
     *      enum={"published", "draft", "suspended"}
     *      example="published"
     * )
     */

    //Add this method to the Controller class
    protected function respondWithToken($token)
    {
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_in' => Auth::factory()->getTTL() * 60
        ], 200);
    }

}
