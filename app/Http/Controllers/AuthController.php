<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\UsersAuthentication;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use Carbon\Carbon;
use App\Models\Users;
use App\Models\UsersAddress;
use App\Models\UsersPhotos;


class AuthController extends Controller
{

    /**
     * @OA\Post(
     * tags={"Authentication"},
     *  path="/api/login",
     *  operationId="testing",
     *  summary="Get Testing",
     *       @OA\Parameter(
     *          name="mytest",
     *          in="path",
     *          required=true,
     *      ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error"),

     * )
     */



    public function login(Request $request)
    {

        //validate incoming request
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     *
     * Create Building Photo
     *
     * @SWG\Post(
     *     path="/api/v1/buildings/{buildingId}/photo",
     *     security={{"oauth2": {"*"}}},
     *     tags={"building"},
     *     consumes={"text/plain", "application/json"},
     *     produces={"text/plain", "application/json"},
     *     description="Update building",
     *     @SWG\Parameter(
     *         name="buildingId",
     *         in="path",
     *         description="Building id",
     *         required=true,
     *         type="number",
     *     ),
     *     @SWG\Parameter(
     *          name="photo",
     *          in="formData",
     *          required=true,
     *          description="Building photo",
     *          type="file",
     *     ),
     *     @SWG\Response( response=200, description="ok"),
     *     @SWG\Response( response=404, description="Building not found"),
     * )
     *
     */

    public function register(Request $request)
    {

        if (!empty($request->json()->all())) {
            $json = $request->getContent();
            $request->request->add(json_decode($json, true));
        }
        //validate incoming request
        $this->validate($request, [
            'firstName' => 'required|string',
            'email' => 'required|email|unique:UsersAuthentication',
            'password' => 'required|confirmed'
        ]);

        $userDescription = [
            'document' => $request->document,
            'dateBirth' => $request->dateBirth,
            'gender' => $request->gender,
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'telefone' => $request->telefone,
            'ong' => $request->ong
        ];


          $userAddress = [
            'address' => !isset($request->address) ? "-" : $request->address,
            'number' => $request->number,
            'neighborhood' => $request->neighborhood,
            'city' => $request->city,
            'state' => $request->state,
            'postalCode' => $request->postalCode,
            'country' => $request->country
        ];

        try{

            $addressId = UsersAddress::create($userAddress)->id;
            $userDescription["addressId"] = $addressId;
            $userId = Users::create($userDescription)->id;

            if($request->hasFile('photo')) {
                $img = ImageService::up($request, 'photo', "", $userId);

                $userPhoto = [
                    'desc' => !isset($request->desc) ? '-' : $request->desc,
                    'bucketName' => env("AWS_BUCKET"),
                    'userId' => "$userId",
                    'key' => $img['path']
                ];
                UsersPhotos::create($userPhoto)->id;
            }
            $register = new UsersAuthentication();
            $register->email = $request->input('email');
            $register->password = app('hash')->make($request->input('password'));
            $register->userId = $userId;
            $register->save();

          $select = DB::select(" SELECT a.authId, u.firstName, u.lastName, a.email FROM UsersAuthentication a
                    inner JOIN Users u ON a.userId = u.userId
                     where u.userId = $userId");

            return response()->json(['user' => $select, 'message' => 'CREATED'], 201);

        } catch (Exception $e) {
            //return error message
            return $e->getMessage();;
        }


    }

    /**
     * @OA\Post(
     * tags={"Authentication"},
     *  path="/api/logout",
     *  operationId="testing",
     *  summary="Get Testing",
     *       @OA\Parameter(
     *          name="mytest",
     *          in="path",
     *          required=true,
     *      ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error"),

     * )
     */

    public function logout () {
        Auth::logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

//    public function alter(Request $request)
//    {
//
//        if (!empty($request->json()->all())) {
//            $json = $request->getContent();
//            $request->request->add(json_decode($json, true));
//        }
//        //validate incoming request
//        $this->validate($request, [
//            'firstName' => 'required|string',
//            'email' => 'required|email|unique:UsersAuthentication',
//            'password' => 'required|confirmed'
//        ]);
//
//        $userDescription = [
//            'document' => $request->document,
//            'dateBirth' => $request->dateBirth,
//            'gender' => $request->gender,
//            'firstName' => $request->firstName,
//            'lastName' => $request->lastName,
//            'telefone' => $request->telefone,
//            'ong' => $request->ong
//        ];
//
//
//        $userAddress = [
//            'address' => !isset($request->address) ? "" : $request->address,
//            'number' => $request->number,
//            'neighborhood' => $request->neighborhood,
//            'city' => $request->city,
//            'state' => $request->state,
//            'postalCode' => $request->postalCode,
//            'country' => $request->country
//        ];
//
//        try{
//
//            $addressId = UsersAddress::update($userAddress)->id;
//            $userDescription["addressId"] = $addressId;
//            $userId = Users::update($userDescription)->id;
//
//            if($request->hasFile('photo')) {
//                $img = ImageService::up($request, 'photo', "", $userId);
//
//                $userPhoto = [
//                    'desc' => !isset($request->desc) ? '-' : $request->desc,
//                    'bucketName' => env("AWS_BUCKET"),
//                    'userId' => "$userId",
//                    'key' => $img['path']
//                ];
//                UsersPhotos::create($userPhoto)->id;
//            }
//
//            $select = DB::select(" SELECT a.authId, u.firstName, u.lastName, a.email FROM UsersAuthentication a
//                    inner JOIN Users u ON a.userId = u.userId
//                     where u.userId = $userId");
//
//            return response()->json(['user' => $select, 'message' => 'CREATED'], 201);
//
//        } catch (Exception $e) {
//            //return error message
//            return $e->getMessage();;
//        }
//
//
//    }

}
