<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use  App\Models\UsersAuthentication;
use Illuminate\Support\Facades\DB;

class UsersAuthenticationController extends Controller
{
     /**
     * Instantiate a new UserController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @OA\Get(
     * tags={"User"},
     *  path="/api/profile",
     *  operationId="testing",
     *  summary="Get Testing",
     *       @OA\Parameter(
     *          name="mytest",
     *          in="path",
     *          required=true,
     *      ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error"),

     * )
     */
    public function profile()
    {
        return response()->json(['user' => Auth::user()], 200);
    }
    /**
     * @OA\Get(
     * tags={"User"},
     *  path="/api/users",
     *  operationId="testing",
     *  summary="Get Testing",
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error"),

     * )
     */
    public function allUsers()
    {

        try {
            $user = DB::select("
            SELECT u.userId, u.document, u.firstName, u.lastName, u.dateBirth, u.gender, u.telefone, u.ong, a.authId,
       a.email,ua.city, ua.state, p.key, p.desc FROM UsersAuthentication a
           inner JOIN Users u ON a.userId = u.userId
    inner join UsersAddress ua on ua.addressId = u.addressId inner join UsersPhotos p on p.userId = u.userId
            ");

            return response()->json(['users' => $user], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }
    }

    /**
     * @OA\Get(
     * tags={"User"},
     *  path="/api/users/{id}",
     *  operationId="testing",
     *  summary="Get Testing",
     *   security={{ "Bearer":{} }},
     *       @OA\Parameter(
     *          name="id",
     *          in="path",
     *          description="User Id",
     *          @OA\Schema(
     *              type="integer",
     *              format="int64"
     *          ),
     *          required=true,
     *          example="1"
     *      ),
     *   @OA\Response(response=200, description="successful operation"),
     *   @OA\Response(response=406, description="not acceptable"),
     *   @OA\Response(response=500, description="internal server error"),

     * )
     */
    public function singleUser($id)
    {
        try {
                $user = DB::select(" SELECT u.userId, u.document, u.firstName, u.lastName, u.dateBirth, u.gender, u.telefone, u.ong,
                    a.authId, a.email,
                    ua.address, ua.neighborhood, ua.city, ua.state
                FROM UsersAuthentication a
                inner JOIN Users u ON a.userId = u.userId
                inner JOIN UsersAddress ua ON u.addressId = ua.addressId
                where u.userId = $id");

            return response()->json(['user' => $user], 200);

        } catch (\Exception $e) {

            return response()->json(['message' => 'user not found!'], 404);
        }

    }

//    public function ()
//    {
//        return response()->json(['user' => Auth::user()], 200);
//    }

}
