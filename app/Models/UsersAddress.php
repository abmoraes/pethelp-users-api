<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UsersAddress extends Model {

    public $table = "UsersAddress";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;

    protected $fillable = [
        'addressId', 'address', 'number', 'neighborhood','city', 'state', 'postalCode','country'
    ];
}
