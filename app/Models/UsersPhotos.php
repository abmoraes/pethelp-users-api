<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UsersPhotos extends Model {

    public $table = "UsersPhotos";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = [
        'userId','key', 'desc', 'bucketName', 'createdAt'
    ];
}
